package com.example.demo.batch.shpping.config;

import com.example.demo.batch.shpping.dao.Items;
import com.example.demo.batch.shpping.dao.Users;
import com.example.demo.batch.shpping.repository.ItemsJpaRepository;
import com.example.demo.batch.shpping.repository.UserJpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class ShoppingConfig {

    @Autowired
    private UserJpaRepository userJpaRepository;

    @Autowired
    private ItemsJpaRepository itemsJpaRepository;

    @Bean
    public List<Users> users(){return userJpaRepository.findAll();}

    @Bean
    public List<Items> items(){
        return  itemsJpaRepository.findAll();
    }
}
