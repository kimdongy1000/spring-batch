package com.example.demo.batch.shpping.repository;

import com.example.demo.batch.shpping.dao.Purchases;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface PurchasesJpaRepository extends JpaRepository<Purchases , String> {

    List<Purchases> findByPurchaseIdDtsAfter(Date purchaseIdDts);
}
