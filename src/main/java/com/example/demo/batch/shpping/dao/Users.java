package com.example.demo.batch.shpping.dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "Users")
public class Users {

    @Id
    @Column(name = "UserID")
    private int userID;
    @Column(name = "UserName")
    private String userName;

    @Override
    public String toString() {
        return "Users{" + "userID=" + userID + ", userName='" + userName + '\'' + '}';
    }
}
