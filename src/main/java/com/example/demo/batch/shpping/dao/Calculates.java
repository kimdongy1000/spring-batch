package com.example.demo.batch.shpping.dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "Calculates")
public class Calculates {

    @Id
    @Column(name = "CalculateId")
    private String calculateId;
    @Column(name = "UserId")
    private int userId;

    @Column(name = "ItemId")
    private int ItemId;

    @Column(name = "PurchaseId")
    private int purchaseId;

    @Column(name = "UserName")
    private String userName;

    @Column(name = "ItemName")
    private String itemName;

    @Column(name = "ItemType")
    private String itemType;

    @Column(name = "ItemPrice")
    private int itemPrice;

    @Column(name = "PurchaseIdDts")
    private Date purchaseIdDts;
}
