package com.example.demo.batch.shpping.repository;

import com.example.demo.batch.shpping.dao.Calculates;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CalculatesJpaRepository extends JpaRepository<Calculates , String> {
}
