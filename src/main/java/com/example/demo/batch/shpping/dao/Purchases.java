package com.example.demo.batch.shpping.dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "Purchases")
public class Purchases {


    @Id
    @Column(name = "PurchaseId")
    private String purchaseId;
    @Column(name = "UserID")
    private int userId;

    @Column(name = "ItemId")
    private int itemId;

    @Column(name = "PurchaseIdDts")
    private Date purchaseIdDts;


    @Override
    public String toString() {
        return "Purchases{" + "userId=" + userId + ", itemId=" + itemId + ", purchaseId=" + purchaseId + ", purchaseIdDts=" + purchaseIdDts + '}';
    }
}
