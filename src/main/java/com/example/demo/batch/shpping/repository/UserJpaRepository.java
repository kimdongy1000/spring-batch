package com.example.demo.batch.shpping.repository;

import com.example.demo.batch.shpping.dao.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserJpaRepository extends JpaRepository<Users , Integer> {

}
