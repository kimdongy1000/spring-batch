package com.example.demo.batch.shpping.dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "Items")
public class Items {


    @Id
    @Column(name = "ItemId")
    private int itemId;

    @Column(name = "ItemName")
    private String itemName;

    @Column(name = "ItemType")
    private String itemType;
    @Column(name = "ItemPrice")
    private int itemPrice;

    @Override
    public String toString() {
        return "Items{" + "itemId=" + itemId + ", itemName='" + itemName + '\'' + ", itemType='" + itemType + '\'' + ", itemPrice=" + itemPrice + '}';
    }
}

