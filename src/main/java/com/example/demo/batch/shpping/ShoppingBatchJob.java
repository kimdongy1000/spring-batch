package com.example.demo.batch.shpping;

import com.example.demo.batch.shpping.dao.Items;
import com.example.demo.batch.shpping.dao.Purchases;
import com.example.demo.batch.shpping.dao.Users;
import com.example.demo.batch.shpping.repository.PurchasesJpaRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Configuration
@RequiredArgsConstructor
public class ShoppingBatchJob {

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    private PurchasesJpaRepository purchasesRepository;

    @Autowired
    private final List<Users> users;

    @Autowired
    private final List<Items> items;



    @Bean
    public Job shoppingJob(Step shoppingStep1){

        return jobBuilderFactory.get("shoppingJob")
                .incrementer(new RunIdIncrementer())
                .start(shoppingStep1)
                .build();
    }

    @Bean
    @JobScope
    public Step shoppingStep1(){

        return stepBuilderFactory.get("shoppingStep1")
                .tasklet(new Tasklet() {
                    @Override
                    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {

                        Random userRnd = new Random();
                        Random itemRnd = new Random();


                        List<Purchases> purchasesList = new ArrayList<>();
                        String todayFormat = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));


                        for (int i = 0; i < 10000; i++) {

                            int user_rndNumber = userRnd.nextInt(4);
                            int item_rndNumber = itemRnd.nextInt(32);

                            Users rndUser = users.get(user_rndNumber);
                            Items rndItems = items.get(item_rndNumber);

                            purchasesList.add(new Purchases(todayFormat +"_"+ UUID.randomUUID().toString()  , rndUser.getUserID() , rndItems.getItemId() ,  new Date()));

                        }

                       purchasesRepository.saveAll(purchasesList);


                        return RepeatStatus.FINISHED;
                    }


                }).build();

    }
}
