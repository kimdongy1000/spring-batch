package com.example.demo.batch.shpping;

import com.example.demo.batch.shpping.dao.Calculates;
import com.example.demo.batch.shpping.dao.Items;
import com.example.demo.batch.shpping.dao.Purchases;
import com.example.demo.batch.shpping.dao.Users;
import com.example.demo.batch.shpping.repository.CalculatesJpaRepository;
import com.example.demo.batch.shpping.repository.ItemsJpaRepository;
import com.example.demo.batch.shpping.repository.PurchasesJpaRepository;
import com.example.demo.batch.shpping.repository.UserJpaRepository;
import lombok.AllArgsConstructor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.data.builder.RepositoryItemReaderBuilder;
import org.springframework.batch.item.data.builder.RepositoryItemWriterBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Sort;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.UUID;

@Configuration
@AllArgsConstructor
public class ShoppingBatchJob2 {

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    private PurchasesJpaRepository purchasesRepository;

    @Autowired
    private ItemsJpaRepository itemsJpaRepository;

    @Autowired
    private UserJpaRepository userJpaRepository;

    @Autowired
    private CalculatesJpaRepository calculatesJpaRepository;


    @Bean
    public Job shoppingJob2(Step shoppingStep2 ){

        return jobBuilderFactory.get("shoppingJob2")
                .incrementer(new RunIdIncrementer())
                .start(shoppingStep2)
                .build();
    }

    @Bean
    @JobScope
    public Step shoppingStep2(ItemReader<Purchases> purchasesReader , ItemProcessor<Purchases , Calculates> converterData , ItemWriter<Calculates> calculatesWriter){

        return stepBuilderFactory.get("shoppingStep2")
                .<Purchases , Calculates >chunk(100)
                .reader(purchasesReader)
                .processor(converterData)
                .writer(calculatesWriter)
                .build();

    }

    @Bean
    @StepScope
    public ItemReader<Purchases> purchasesReader(){

        return new RepositoryItemReaderBuilder<Purchases>()
                .name("purchasesReader")
                .repository(purchasesRepository)
                .methodName("findAll")
                .pageSize(100)
                .sorts(Collections.singletonMap("purchaseId" , Sort.Direction.ASC))
                .build();

    }

    @Bean
    @StepScope
    public ItemProcessor<Purchases , Calculates> converterData(){

        final String todayFormat = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));

        return purchases -> {

            Items items = itemsJpaRepository.findById(purchases.getItemId()).get();
            Users users = userJpaRepository.findById(purchases.getUserId()).get();



            return new Calculates(todayFormat + "-" + users.getUserID() + "-" + items.getItemName() + "-" + UUID.randomUUID()
                                    ,users.getUserID()
                                    ,items.getItemId()
                                    ,purchases.getItemId()
                                    ,users.getUserName()
                                    ,items.getItemName()
                                    ,items.getItemType()
                                    ,items.getItemPrice()
                                    ,purchases.getPurchaseIdDts()
            );
        };

    }

    @Bean
    @StepScope
    public ItemWriter<Calculates> calculatesWriter(){

        return new RepositoryItemWriterBuilder<Calculates>()
                .repository(calculatesJpaRepository)
                .methodName("save")
                .build();
    }
}
