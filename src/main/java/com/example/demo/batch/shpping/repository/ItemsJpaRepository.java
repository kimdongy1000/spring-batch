package com.example.demo.batch.shpping.repository;

import com.example.demo.batch.shpping.dao.Items;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemsJpaRepository extends JpaRepository<Items , Integer> {
}
