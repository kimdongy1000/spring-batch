package com.example.demo.batch.listener;

import lombok.RequiredArgsConstructor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class ListenerBatch {

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Bean
    public Job listenerBatchJob(Step listenerBachStep){

        return jobBuilderFactory.get("listenerBatchJob")
                .incrementer(new RunIdIncrementer())
                .start(listenerBachStep)
                .listener(new JobListener())
                .build();
    }

    @Bean
    @JobScope
    public Step listenerBachStep(){

        return stepBuilderFactory.get("listenerBachStep")
                .tasklet(new Tasklet() {
                    @Override
                    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {

                        for(int i = 0; i < 10; i++){
                            if(i == 8) throw new RuntimeException("No more than 8");
                            System.out.println("Time is " + i);

                        }
                        return RepeatStatus.FINISHED;
                    }
                }).build();
    }
}
