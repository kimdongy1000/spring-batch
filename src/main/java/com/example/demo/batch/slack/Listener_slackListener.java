package com.example.demo.batch.slack;

import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class Listener_slackListener implements JobExecutionListener {

    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH시 mm분 ss초");
    private Slack_Message_Service slackMessageService;

    public Listener_slackListener(Slack_Message_Service slackMessageService){
        this.slackMessageService = slackMessageService;
    }



    @Override
    public void beforeJob(JobExecution jobExecution) {

        String text = LocalDate.now() + " " + LocalTime.now().format(formatter) + " " +jobExecution.getJobInstance().getJobName() + " 실행되었습니다";
        slackMessageService.sendMessageToSlack(text);

    }

    @Override
    public void afterJob(JobExecution jobExecution) {

        String text = LocalDate.now() + " " + LocalTime.now().format(formatter) + " " +jobExecution.getJobInstance().getJobName();

        if (jobExecution.getStatus().isUnsuccessful()) {
            text = text+ "job 실행이 실패 되었습니다";
        } else {
            text = text+ "job 실행이 성공 되었습니다";

        }

        slackMessageService.sendMessageToSlack(text);


    }
}
