package com.example.demo.batch.slack;

import lombok.RequiredArgsConstructor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class Listener_SlackBatch {

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    private Slack_Message_Service slackMessageService;

    @Bean
    public Job listener_slackBatch(Step listener_slackStep){

        return jobBuilderFactory.get("listener_slackBatch")
                .incrementer(new RunIdIncrementer())
                .start(listener_slackStep)
                .listener(new Listener_slackListener(slackMessageService))
                .build();
    }

    @Bean
    public Step listener_slackStep(){

        return stepBuilderFactory.get("listener_slackStep")
                .tasklet(new Tasklet() {
                    @Override
                    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
                        for(int i = 0; i < 10; i++){
                            System.out.println("i : count: " +   i);
                        }
                        return RepeatStatus.FINISHED;
                    }
                }).build();
    }
}
