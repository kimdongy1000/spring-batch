package com.example.demo.batch.slack;

import com.slack.api.Slack;
import com.slack.api.methods.MethodsClient;
import com.slack.api.methods.request.chat.ChatPostMessageRequest;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Slack_Message_Service {

    @Value("${slack.token}")
    private String token;

    public void sendMessageToSlack(String message){

        String slack_chanel_name = "#배치모니터링";

        try{

            MethodsClient methods = Slack.getInstance().methods(token);

            ChatPostMessageRequest request = ChatPostMessageRequest.builder()
                    .channel(slack_chanel_name)
                    .text(message)
                    .build();

            methods.chatPostMessage(request);



        }catch(Exception e){
            throw new RuntimeException(e);
        }

    }
}
