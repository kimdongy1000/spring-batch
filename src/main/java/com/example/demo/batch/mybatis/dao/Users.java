package com.example.demo.batch.mybatis.dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@NoArgsConstructor
@AllArgsConstructor

public class Users {


    private int userID;

    private String userName;

    @Override
    public String toString() {
        return "Users{" + "userID=" + userID + ", userName='" + userName + '\'' + '}';
    }
}
