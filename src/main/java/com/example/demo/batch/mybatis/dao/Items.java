package com.example.demo.batch.mybatis.dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Items {



    private int itemId;


    private String itemName;


    private String itemType;

    private int itemPrice;

    @Override
    public String toString() {
        return "Items{" + "itemId=" + itemId + ", itemName='" + itemName + '\'' + ", itemType='" + itemType + '\'' + ", itemPrice=" + itemPrice + '}';
    }
}

