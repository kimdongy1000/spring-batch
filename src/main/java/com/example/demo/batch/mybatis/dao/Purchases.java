package com.example.demo.batch.mybatis.dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Purchases {



    private String purchaseId;

    private int userId;


    private int itemId;


    private Date purchaseIdDts;


    @Override
    public String toString() {
        return "Purchases{" + "userId=" + userId + ", itemId=" + itemId + ", purchaseId=" + purchaseId + ", purchaseIdDts=" + purchaseIdDts + '}';
    }
}
