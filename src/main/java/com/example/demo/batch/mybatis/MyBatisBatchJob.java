package com.example.demo.batch.mybatis;


import com.example.demo.batch.mybatis.dao.Calculates;
import com.example.demo.batch.mybatis.dao.Items;
import com.example.demo.batch.mybatis.dao.Purchases;
import com.example.demo.batch.mybatis.dao.Users;
import lombok.RequiredArgsConstructor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.batch.builder.MyBatisBatchItemWriterBuilder;
import org.mybatis.spring.batch.builder.MyBatisPagingItemReaderBuilder;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.UUID;

@Configuration
@RequiredArgsConstructor
public class MyBatisBatchJob {

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    private SqlSessionFactory sqlSessionFactory;



    @Bean
    public Job mybatisJob(Step myBatisStep){

        return jobBuilderFactory.get("mybatisJob")
                .incrementer(new RunIdIncrementer())
                .start(myBatisStep)
                .build();
    }

    @Bean
    @JobScope
    public Step myBatisStep(ItemReader<Purchases> mybatisPurchasesReader , ItemProcessor<Purchases , Calculates> mybatisConvertData , ItemWriter<Calculates> calculatesItemWriter){

        return stepBuilderFactory.get("myBatisStep")
                .<Purchases , Calculates>chunk(100)
                .reader(mybatisPurchasesReader)
                .processor(mybatisConvertData)
                .writer(calculatesItemWriter)
                .build();
    }


    /*
    * ItemReader 이때 MyBatisPagingItemReaderBuilder 를 사용해서 데이터를 가져올것이고
    * MyBatisPagingItemReaderBuilder 를 사용해서 불러옵니다
    *
    * sqlSessionFactory 는 우리가 Batch 를 사용할려고 하는 sqlSessionBean 을 넣어줍니다
    *
    * queryId 는 xml 에 매핑이 되는 id 로 원천 데이터를 불러올 쿼리문입니다
    *
    * pageSize 는 chunk 와 마찬가지로 한번에 처리할 값을 적어둡니다
    *
    * */
    @Bean
    @StepScope
    public ItemReader<Purchases> mybatisPurchasesReader(){

        return new MyBatisPagingItemReaderBuilder<Purchases>()
                .sqlSessionFactory(sqlSessionFactory)
                .queryId("spring.batch.mybatis.batch.select_batch_purchases")
                .pageSize(100)
                .build();
    }

    /*
    *
    * ItemProcessor 우리는 JPA 에서 처리한것과 마찬가지로 데이터를 불러와서 가공을 한뒤에 처리할 예정입니다
    *
    * repository 를 따로 만들지 말고 위에서 정의한 sqlSessionFactory 에서 동일한 session 을 가져온뒤 xml 에 매핑이 되는 namespace 를 적어줍니다
    * 이렇게 하는 이유는 아래서 설명을 드리겠습니다
    *
    *
    *
    *
    * */
    @Bean
    @StepScope
    public ItemProcessor<Purchases , Calculates> mybatisConvertData(){

        final String todayFormat = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));

        return new ItemProcessor<Purchases, Calculates>() {
            @Override
            public Calculates process(Purchases item) throws Exception {



                int userId = item.getUserId();
                int itemId = item.getItemId();

                //Items items = itemRepository.findByItemId(itemId);
                //Users users = userRepository.findByUserId(userId);

                Items items = sqlSessionFactory.openSession().selectOne("com.example.demo.batch.mybatis.repository.ItemRepository.findByItemId" , itemId);
                Users users = sqlSessionFactory.openSession().selectOne("com.example.demo.batch.mybatis.repository.UserRepository.findByItemId" , userId);


                Calculates calculates = new Calculates(todayFormat + "-" + users.getUserID() + "-" + items.getItemName() + "-" + UUID.randomUUID()
                        ,users.getUserID()
                        ,items.getItemId()
                        ,item.getItemId()
                        ,users.getUserName()
                        ,items.getItemName()
                        ,items.getItemType()
                        ,items.getItemPrice()
                        ,item.getPurchaseIdDts());

                return calculates;
            }
        };

    }

    /*
    *
    * 받은 데이터를 insert 하는 문구입니다
    * ItemReader 하고는 조금 다르게 MyBatisBatchItemWriterBuilder 사용합니다
    *
    * sqlSessionFactory 사용할 DB 연결 객체를 넣어주시고
    *
    * statementId insert 할 쿼리문을 만들어줍니다
    *
    * */
    @Bean
    public ItemWriter<Calculates> mybatisItemWriter(){

        return new MyBatisBatchItemWriterBuilder<Calculates>()
                .sqlSessionFactory(sqlSessionFactory)
                .statementId("spring.batch.mybatis.batch.insert_Calculates")
                .build();
    }




}
