package com.example.demo.batch.mybatis.dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor

public class Calculates {


    private String calculateId;

    private int userId;


    private int ItemId;


    private int purchaseId;


    private String userName;


    private String itemName;


    private String itemType;


    private int itemPrice;


    private Date purchaseIdDts;
}
