package com.example.demo.batch.listener_email;


import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;


import javax.mail.Message;
import javax.mail.internet.MimeMessage;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Properties;


public class Listener_emailListenerBatch implements JobExecutionListener {

    private JavaMailSenderImpl javaMailSender;

    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH시 mm분 ss초");


    public Listener_emailListenerBatch(){

    }

    public Listener_emailListenerBatch(JavaMailSenderImpl javaMailSender){

        this.javaMailSender = javaMailSender;
    }



    @Override
    public void beforeJob(JobExecution jobExecution) {

        try{
            MimeMessage message = javaMailSender.createMimeMessage();
            MimeMessageHelper messageHelper = new MimeMessageHelper(message, false, "UTF-8");

            String subject = LocalDate.now() + " " + LocalTime.now().format(formatter) + " " +jobExecution.getJobInstance().getJobName() + "시작 알림 메일";
            String text = LocalDate.now() + " " + LocalTime.now().format(formatter) + " " +jobExecution.getJobInstance().getJobName() + " 실행되었습니다";

            messageHelper.setTo("kimdongy1000@naver.com");
            messageHelper.setSubject(subject);
            messageHelper.setText(text);

            javaMailSender.send(message);

        }catch(Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public void afterJob(JobExecution jobExecution) {


        try{
            MimeMessage message = javaMailSender.createMimeMessage();
            MimeMessageHelper messageHelper = new MimeMessageHelper(message, false, "UTF-8");
            String subject = LocalDate.now() + " " + LocalTime.now().format(formatter) + " " +jobExecution.getJobInstance().getJobName();
            String text = LocalDate.now() + " " + LocalTime.now().format(formatter) + " " +jobExecution.getJobInstance().getJobName();

            if (jobExecution.getStatus().isUnsuccessful()) {

                subject = subject + "실행 실패 알림";
                text = text+ "job 실행이 실패 되었습니다";

            } else {

                subject = subject + "실행 성공 알림";
                text = text+ "job 실행이 성공 되었습니다";

            }

            messageHelper.setTo("kimdongy1000@naver.com");
            messageHelper.setSubject(subject);
            messageHelper.setText(text);

            javaMailSender.send(message);


        }catch(Exception e){
            e.printStackTrace();
        }




    }
}
