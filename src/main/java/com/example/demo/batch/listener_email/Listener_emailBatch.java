package com.example.demo.batch.listener_email;


import lombok.RequiredArgsConstructor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

@Configuration
@RequiredArgsConstructor
public class Listener_emailBatch {

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    private JavaMailSenderImpl javaMailSender;


    @Bean
    public Job listener_emailJob(Step listener_emailStep){

        return jobBuilderFactory.get("listener_emailJob")
                .incrementer(new RunIdIncrementer())
                .start(listener_emailStep)
                .listener(new Listener_emailListenerBatch(javaMailSender))
                .build();
    }

    @Bean
    @JobScope
    public Step listener_emailStep(){

        return stepBuilderFactory.get("listener_emailStep")
                .tasklet(new Tasklet() {
                    @Override
                    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
                        for(int i = 0; i < 10; i++){

                            //if(i == 8) throw new RuntimeException("No more than 8");
                            System.out.println("Time is Email Count " + i);

                        }
                        return RepeatStatus.FINISHED;

                    }
                }).build();
    }
}
