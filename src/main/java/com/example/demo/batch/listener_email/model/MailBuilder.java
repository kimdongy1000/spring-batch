package com.example.demo.batch.listener_email.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class MailBuilder {

    private String to;

    private String subject;

    private String text;

    private String from;







}
