package com.example.demo.batch.listener_email;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

@Configuration
public class MailConfig {

    @Value("${spring.mail.protocol}")
    private String protocol;

    @Value("${spring.mail.host}")
    private String host;

    @Value("${spring.mail.port}")
    private int port;

    @Value("${spring.mail.username}")
    private String username;

    @Value("${spring.mail.password}")
    private String password;

    @Value("${spring.mail.default-encoding}")
    private String default_encoding;

    @Value("${spring.mail.properties.mail.smtp.auth}")
    private String auth;

    @Value("${spring.mail.properties.mail.smtp.starttls.enable}")
    private String enable;






    @Bean
    public JavaMailSenderImpl javaMailSenderImpl(Properties mailProperties){

        JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();


        javaMailSender.setHost(host);
        javaMailSender.setPort(port);
        javaMailSender.setUsername(username);
        javaMailSender.setPassword(password);

        javaMailSender.setJavaMailProperties(mailProperties);

        return javaMailSender;
    }

    @Bean
    public Properties mailProperties (){

        Properties properties = new Properties();
        properties.setProperty("mail.smtp.auth" , auth);
        properties.setProperty("mail.smtp.starttls.enable" , enable);

        return properties;
    }
}
