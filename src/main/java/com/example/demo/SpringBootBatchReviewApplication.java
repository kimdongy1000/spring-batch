package com.example.demo;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.mail.internet.MimeMessage;

@SpringBootApplication
@EnableBatchProcessing
@Controller
public class SpringBootBatchReviewApplication {

	@Autowired
	private JavaMailSenderImpl javaMailSender;


	public static void main(String[] args) {
		SpringApplication.run(SpringBootBatchReviewApplication.class, args);
	}

	@GetMapping("/sendMail")
	public ResponseEntity<?> sendMail()
	{
		try{


			MimeMessage message = javaMailSender.createMimeMessage();
			MimeMessageHelper messageHelper = new MimeMessageHelper(message, false, "UTF-8");

			messageHelper.setTo("kimdongy1000@naver.com");     // 받는사람 이메일
			messageHelper.setSubject("Bach 가 시작되었습니다"); // 메일제목은 생략이 가능하다
			messageHelper.setText("Spring Batch 테스트 메일" , false);  // 메일 내용


			javaMailSender.send(message);


			return new ResponseEntity<>(null , HttpStatus.OK);
		}catch(Exception e){
			throw new RuntimeException(e);
		}

	}

}
